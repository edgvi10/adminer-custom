# adminer_custom
My Custom Adminer Instalation with @Pematon plugins and others

Plugins for Adminer
===================

Usefull plugins for Adminer database tool (http://www.adminer.org/).

- [Adminer](https://github.com/vrana/adminer/)
- [Slugfy](https://raw.githubusercontent.com/vrana/adminer/master/plugins/slugify.php)
- [TablesFilter](https://raw.githubusercontent.com/vrana/adminer/master/plugins/tables-filter.php)
- [AdminerLoginServers](https://github.com/pematon/adminer-plugins#adminerloginservers)
- [AdminerJsonPreview](https://github.com/pematon/adminer-plugins#adminerjsonpreview)
- [AdminerSimpleMenu](https://github.com/pematon/adminer-plugins#adminersimplemenu)
- [AdminerCollations](https://github.com/pematon/adminer-plugins#adminercollations)
- [How to use](https://github.com/pematon/adminer-plugins#how-to-use)